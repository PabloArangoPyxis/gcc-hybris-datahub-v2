package com.github.skhatri.s3aws.client

import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.CannedAccessControlList
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.S3Object
import org.joda.time.LocalDateTime

public class S3Client {
    private final AmazonS3Client s3Client;

    private static final String AMZ_REDIRECT_LINK = "x-amz-website-redirect-location";
    private static final String PART_EXTENSION = ".part";

    public S3Client(String accessKey, String secretKey, String region) {
        s3Client = new AmazonS3Client(new BasicAWSCredentials(accessKey, secretKey));
        if (region != null && !"".equals(region)) {
            s3Client.setRegion(Region.getRegion(Regions.fromName(region)))
        }
    }

    /*
    * Get checksum (MD5 the content) of the specified file
    */
    public String getChecksum(String bucketName, String key) {
    	try {
    				S3Object object = getS3Object(bucketName, key);
            return object.getObjectMetadata().getContentMD5();
        } catch (Exception e) {
        		// return null in case problems arise (eg: file was not found)
        		return null;
        }
    }

		/*
    * Download a file
    */
    public void downloadFile(String bucketName, String key, String saveTo) {
        try {
        		S3Object object = getS3Object(bucketName, key);

            /////////////////
            // variables to display progress //
            //amount of chunks we need to download
            int count = 0;
            int bufferSizeInBytes = 1024;
            int chunks = object.getObjectMetadata().getContentLength() / bufferSizeInBytes; // how many chunks we download
            int currentChunk = 1; // current chunk
            int lastPercentage = 0; // current chunk


            InputStream inputStream = object.getObjectContent();
            byte[] buf = new byte[bufferSizeInBytes];
            OutputStream out = new FileOutputStream(new File(saveTo + PART_EXTENSION));
            int sizeInMb = object.getObjectMetadata().getContentLength() / 1024 / 1024;
            println "Download size of '${key}' is: $sizeInMb MB (${sizeInMb/1024} GB) "
            while( (count = inputStream.read(buf)) != -1)
            {
               if( Thread.interrupted() )
               {
                   throw new InterruptedException();
               }
               out.write(buf, 0, count);
               int actualPercentage = ((currentChunk*100)/chunks).toInteger();
               if(actualPercentage > lastPercentage){
               	lastPercentage = actualPercentage;
               	out.flush();
               	println "Downloading progress: ${actualPercentage}%"
               }
               currentChunk += 1;
            }
            out.close();
            inputStream.close();

            File part = new File(saveTo + PART_EXTENSION)
            part.renameTo(saveTo)
        } catch (Exception e) {
            throw new RuntimeException("Exception while downloading file from S3: " + e.getMessage());
        }
    }

    private S3Object getS3Object(String bucketName, String key)
    {
    	S3Object object = s3Client.getObject(bucketName, key);
      String redirect = object.getObjectMetadata().getRawMetadataValue(AMZ_REDIRECT_LINK)
      if (redirect != null && !redirect.isEmpty()) {
          println "getting redirect resource = $redirect"
          object = s3Client.getObject(bucketName, redirect.substring(1))
      }
      return object;
    }

    private String createLinkObject(String link, String key, String bucketName) {
        if (link != null && !link.isEmpty()) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setHeader(AMZ_REDIRECT_LINK, createRedirectKey(key));
            metadata.setContentLength(0);
            InputStream inputStream = new ByteArrayInputStream(new byte[0]);
            PutObjectRequest linkPutRequest = new PutObjectRequest(bucketName, link, inputStream, metadata)
            linkPutRequest.setCannedAcl(CannedAccessControlList.Private)
            s3Client.putObject(linkPutRequest);
            return s3Client.generatePresignedUrl(bucketName, link, new LocalDateTime().plusDays(30).toDate());
        }
    }

    private String createRedirectKey(String key) {
        key.startsWith("/") || key.startsWith("http") ? key : "/" + key
    }
}
