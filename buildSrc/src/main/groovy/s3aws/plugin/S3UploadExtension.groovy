package com.github.skhatri.s3aws.plugin

class S3UploadExtension {
    String bucket
    String accessKey = ''
    String secretKey = ''
    String key = ''
    String file = ''
    String link = ''
}
