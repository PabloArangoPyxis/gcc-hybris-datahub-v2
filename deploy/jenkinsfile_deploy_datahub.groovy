#!groovy

properties([
	parameters([		
		string(	name: 'ECR_SERVER',
			   	defaultValue: '960190522109.dkr.ecr.us-east-1.amazonaws.com', 
			   	description: 'ECR Server URL.'),
		string(	name: 'ECR_REGISTRY',
			   	defaultValue: 'gsc-b2b-datahub', 
			   	description: 'ECR registry name.'),		
		string(	name: 'ENVIRONMENT',
			   	defaultValue: 'dev', 
			   	description: 'Environment to deploy to.'),
		string(	name: 'VERSION', 
				defaultValue: 'latest', 
				description: 'DataHub version to deploy.'),
		string(	name: 'ECS_CLUSTER',
			   	defaultValue: 'WEBNP01-HYBDEV-CLUSTER', 
			   	description: 'ECS cluster name.'),
		string(	name: 'SERVICE_NAME',
			   	defaultValue: 'dev-hmhco-gsc-datahub-b2b-app', 
			   	description: 'ECS service name suffix. Environment name will be prefixed.'),
		string(	name: 'TASK_DEFINITION',
			   	defaultValue: 'gmc-b2b-datahub-task-def', 
			   	description: 'ECS Task Definition.'),
		string(	name: 'DESIRED_COUNT',
			   	defaultValue: '1', 
			   	description: 'Number of replicas to deploy.')			   			
		])
	])

node('Linux') {

	checkout scm 

	try {
					
		//Variable set-up
		FILE_LOCATION="./deploy"		
		DESIRED_COUNT = "${params.DESIRED_COUNT}"

		stage('Create new task definition given build'){

			//Replace variables in task definition	
			sh "sed -e \"s;%ENVIRONMENT%;${params.ENVIRONMENT};g\" \
					-e \"s;%VERSION%;${params.VERSION};g\" \
					-e \"s;%ECR_SERVER%;${params.ECR_SERVER};g\" \
					-e \"s;%ECR_REGISTRY%;${params.ECR_REGISTRY};g\" \
					-e \"s;%FAMILY_NAME%;${params.SERVICE_NAME};g\" \
					${FILE_LOCATION}/${params.TASK_DEFINITION}.json > ${FILE_LOCATION}/${params.TASK_DEFINITION}-v${BUILD_NUMBER}.json"

			//Register new task definition revision
			sh "aws ecs register-task-definition --cli-input-json file://${FILE_LOCATION}/${params.TASK_DEFINITION}-v${BUILD_NUMBER}.json --region us-east-1"

		}

		stage('Update service with new task definition'){
			
			TASK_REVISION = sh( 
				script: 'aws ecs describe-task-definition --task-definition ${SERVICE_NAME} --region us-east-1 | egrep "revision" | tr "/" " " | awk \'{print $2}\' | sed \'s/"$//\'',
				returnStdout: true).trim()		

			print "Desired count is ${params.DESIRED_COUNT} and task revision is ${TASK_REVISION}"

			if (DESIRED_COUNT==0) { DESIRED_COUNT=1 }

			//Update service with new task revision and desired count
			sh "aws ecs update-service --cluster ${params.ECS_CLUSTER} --service ${params.SERVICE_NAME} --task-definition ${params.SERVICE_NAME}:${TASK_REVISION} --desired-count ${params.DESIRED_COUNT} --region us-east-1"

		}
	}	
	catch(Exception err){
		currentBuild.result = 'FAILURE'
		throw err
	}
}
