
# hybris-datahub
#Based on hybris-datahub project EY

## Initial project setup

* Checkout the code from repository 
```
git clone git@bitbucket.org:PyxisPortal/gcc-hybris-datahub.git
```
* Cd into the code and checkout dev branch 

```
cd gcc-hybris-datahub 
git checkout develop
```

* Run project setup to download and install datahub and tomcat distributions
```
./gradlew setup
```

* Load up docker with mssql server following this [guide](https://confluence.hmhco.com/x/yJOjCQ). Remember that you need to login into the aws ecr registry before pulling the image, the instructions are also on the guide. 

* Compile the code (local build). Use this command to rebuild your jar files after making changes
```
./gradlew
```
* To startup the server:

```
cd apache-tomcat/bin
./datahubserver.sh
```

* To test the datahub is running, access: http://localhost:29991/datahub-webapp/v1/item-classes/canonical/item-types with credentials `admin / nimda`

## INSTALLATION TUTORIAL
* https://help.hybris.com/6.7.0/hcd/478f702a00104636b16d2614d1db30ac.html
* https://help.hybris.com/6.7.0/hcd/d4f38c211baf4d209dd548f2d9b0afce.html
