#!/bin/bash

REPO=$1
#configure your project folder
ORDER_DH_PROJECT=$REPO/hmhb2b-datahub-order
IDOC_IMPORTER_PROJECT=$REPO/hmhb2b-datahub-idoc-importer
HMH_SAPIDOC_PROJECT=$REPO/hmhb2b-sapidocintegration

#deploy folder, then it will be copyed to apache tomcat
DEPLOY_CUSTOM_LIBS=$REPO/common/lib
DH_SM_VERSION=0.1.0

# set correct maven directory here!
MAVEN_HOME=/opt/maven
PATH=$MAVEN_HOME/bin:$PATH
export PATH MAVEN_HOME
export CLASSPATH=.

MVN=mvn

###################

echo "--> build hybris custom datahub libraries"

# remove old hybris directories
rm -r $ORDER_DH_PROJECT/hmhb2b-datahub-order-canonical/target/* || true
rm -r $ORDER_DH_PROJECT/hmhb2b-datahub-order-raw/target/* || true
rm -r $ORDER_DH_PROJECT/hmhb2b-datahub-order-target/target/* || true
# maven install and build your new lib
cd $ORDER_DH_PROJECT; $MVN clean install -DskipTests; cd -

##################

echo "--> moves generated jar files into lib folder"

cp $ORDER_DH_PROJECT/hmhb2b-datahub-order-canonical/target/hmhb2b-datahub-order-canonical-$DH_SM_VERSION.jar $DEPLOY_CUSTOM_LIBS/hmhb2b-datahub-order-canonical-$DH_SM_VERSION.jar
cp $ORDER_DH_PROJECT/hmhb2b-datahub-order-raw/target/hmhb2b-datahub-order-raw-$DH_SM_VERSION.jar $DEPLOY_CUSTOM_LIBS/hmhb2b-datahub-order-raw-$DH_SM_VERSION.jar
cp $ORDER_DH_PROJECT/hmhb2b-datahub-order-target/target/hmhb2b-datahub-order-target-$DH_SM_VERSION.jar $DEPLOY_CUSTOM_LIBS/hmhb2b-datahub-order-target-$DH_SM_VERSION.jar

echo "--> build idoc importer"

# remove old hybris directories
rm -r $IDOC_IMPORTER_PROJECT/target/* || true
# maven install and build your new lib
cd $IDOC_IMPORTER_PROJECT; $MVN clean install -DskipTests; cd -

##################

echo "--> moves generated jar files into lib folder"

cp $IDOC_IMPORTER_PROJECT/target/hmhb2b-datahub-idoc-importer-$DH_SM_VERSION.jar $DEPLOY_CUSTOM_LIBS/hmhb2b-datahub-idoc-importer-$DH_SM_VERSION.jar

##################

echo "--> build hmh sapidocintegration"

rm -r $HMH_SAPIDOC_PROJECT/hmhb2b-sapidocintegration.jar || true
cd $HMH_SAPIDOC_PROJECT; jar -cf hmhb2b-sapidocintegration.jar META-INF; cd -

cp $HMH_SAPIDOC_PROJECT/hmhb2b-sapidocintegration.jar $DEPLOY_CUSTOM_LIBS/hmhb2b-sapidocintegration.jar