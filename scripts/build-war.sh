#!/bin/sh
# Generates a datahub-webapp.war file containing all out custom .jar
# files and a config folder matching the hCS template

REPO=`pwd`/..

echo "---> Building the extensions, jar files will be copied to common/lib/"
./build-extensions.sh $REPO

echo "---> Extracting default datahub-webapp.war"
cd $REPO
rm -rf datahub-deploy
mkdir datahub-deploy datahub-deploy/bin datahub-deploy/bin/datahub-webapp datahub-deploy/config
DH_WEBAPP=$REPO/datahub-deploy/bin/datahub-webapp

unzip common/lib/datahub-webapp.war -d $DH_WEBAPP

echo "---> Copying our custom .jar files to inside the extracted .war"
cp -v common/lib/* $DH_WEBAPP/WEB-INF/lib/
rm -fv $DH_WEBAPP/WEB-INF/lib/datahub-webapp.war

echo "---> Zipping the war file"
cd $DH_WEBAPP
zip -r datahub-webapp.war ./*
cd $REPO
mv $DH_WEBAPP/datahub-webapp.war $DH_WEBAPP/../
rm -rf $DH_WEBAPP

echo "---> Copying config folders"
CNF=$REPO/configuration/
cp -Rv $CNF/dev/ $REPO/datahub-deploy/config/dev
cp -Rv $CNF/qa/ $REPO/datahub-deploy/config/stag
cp -Rv $CNF/prod/ $REPO/datahub-deploy/config/prod
