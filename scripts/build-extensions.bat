@echo off

rem ---------------------------------------------------------------------------
rem Start script to build datahub locally
rem ---------------------------------------------------------------------------

setlocal

set REPO=%1
set ORDER_DH_PROJECT=%REPO%\hmhb2b-datahub-order
set DH_SM_VERSION=0.1.0

set MVN=mvn

echo build hmh custom datahub libraries

rem remove old hybris directories
if exist %ORDER_DH_PROJECT%\hmhb2b-datahub-order-canonical\target rmdir /s /q %ORDER_DH_PROJECT%\hmhb2b-datahub-order-canonical\target
if exist %ORDER_DH_PROJECT%\hmhb2b-datahub-order-raw\target rmdir /s /q %ORDER_DH_PROJECT%\hmhb2b-datahub-order-raw\target
if exist %ORDER_DH_PROJECT%\hmhb2b-datahub-order-target\target rmdir /s /q %ORDER_DH_PROJECT%\hmhb2b-datahub-order-target\target

cd %ORDER_DH_PROJECT%
call %MVN% clean install -DskipTests

echo moves generated jar files for HMHORDER into lib folder

copy /y %ORDER_DH_PROJECT%\hmhb2b-datahub-order-canonical\target\hmhb2b-datahub-order-canonical-%DH_SM_VERSION%.jar %DEPLOY_CUSTOM_LIBS%\hmhb2b-datahub-order-canonical-%DH_SM_VERSION%.jar
copy /y %ORDER_DH_PROJECT%\hmhb2b-datahub-order-raw\target\hmhb2b-datahub-order-raw-%DH_SM_VERSION%.jar %DEPLOY_CUSTOM_LIBS%\hmhb2b-datahub-order-raw-%DH_SM_VERSION%.jar
copy /y %ORDER_DH_PROJECT%\hmhb2b-datahub-order-target\target\hmhb2b-datahub-order-target-%DH_SM_VERSION%.jar %DEPLOY_CUSTOM_LIBS%\hmhb2b-datahub-order-target-%DH_SM_VERSION%.jar

:end