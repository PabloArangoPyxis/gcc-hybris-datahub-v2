#!/bin/sh

ENV="dev"
REPO=".."
TOMCAT_FOLDER="$REPO/apache-tomcat/"

./build-extensions.sh $REPO
./apply-configuration.sh $ENV $REPO $TOMCAT_FOLDER
