@echo off

rem ---------------------------------------------------------------------------
rem Applying datahub custom configuration to tomcat
rem ---------------------------------------------------------------------------

setlocal

set ENV_FOLDER=%1
set REPO=%2
set TOMCAT_FOLDER=%3

set SOURCE_FOLDER=%REPO%\configuration\%ENV_FOLDER%
set COMMON_FOLDER=%REPO%\common
set LOCAL_PROPERTIES_FILE=local.properties
set WEB_APP_FOLDER=%REPO%\web-app

rem Copy the war file to tomcat folder
echo copying files %WEB_APP_FOLDER% to %TOMCAT_FOLDER%
copy /B %WEB_APP_FOLDER%\datahub-webapp-*.war %TOMCAT_FOLDER%\lib\datahub-webapp.war

rem Copy all the Conf files from selected Environemnt to the Tomcat folder 
echo copying files %SOURCE_FOLDER% to %TOMCAT_FOLDER%
xcopy /s/y %SOURCE_FOLDER%\%LOCAL_PROPERTIES_FILE% %TOMCAT_FOLDER%\lib

rem Copy common files to all environments to the Tomcat folder 
echo copying files %COMMON_FOLDER% to %TOMCAT_FOLDER%
xcopy /s/y %COMMON_FOLDER% %TOMCAT_FOLDER%

:end