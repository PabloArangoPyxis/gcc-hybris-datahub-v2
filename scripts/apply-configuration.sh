#!/bin/sh

ENV_FOLDER=$1
REPO=$2
TOMCAT_FOLDER=$3

SOURCE_FOLDER="$REPO/configuration/$ENV_FOLDER"
COMMON_FOLDER="$REPO/common"
WEB_APP_FOLDER="$REPO/web-app"



# Copy common files to all environments to the Tomcat folder 
echo "copying files $COMMON_FOLDER/* -> $TOMCAT_FOLDER"
cp -r $COMMON_FOLDER/* $TOMCAT_FOLDER


# Copy all the Conf files from selected Environemnt to the Tomcat conf folder 
echo "copying datahub local.properties $SOURCE_FOLDER/datahub -> ${TOMCAT_FOLDER}conf/"
cp -r $SOURCE_FOLDER/ $TOMCAT_FOLDER/conf/
