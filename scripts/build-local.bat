@echo off

rem ---------------------------------------------------------------------------
rem Start script to build datahub locally
rem ---------------------------------------------------------------------------

setlocal

if "%~1"=="" (
    set ENV=local
) else (
    set ENV=%1
)

set REPO=..
set TOMCAT_FOLDER=%REPO%\apache-tomcat\

call build-extensions.bat %REPO%
call apply-configuration.bat %ENV% %REPO% %TOMCAT_FOLDER%

:end