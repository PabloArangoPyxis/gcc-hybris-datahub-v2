#!/bin/sh

if [ $# -ne 1 ]; then
    echo "Setting up default env as local"
    ENV="local"
else
    ENV=$1
fi

REPO=".."
TOMCAT_FOLDER="$REPO/apache-tomcat/"

#./build-extensions.sh $REPO
./apply-configuration.sh $ENV $REPO $TOMCAT_FOLDER
